class While2 {
	public static void main(String[] args) {
		int oddCount = 0;
		int evenCount = 0;
		int rem = 0;
		int num = 214367689;
		while(num > 0) {
			rem = num % 10;
			num = num / 10;
			if(rem % 2 == 0) 
				evenCount++;
			else
				oddCount++;
		}
		System.out.println("OddCount:"+oddCount);
		System.out.println("EvenCount:"+evenCount);
	}
}

