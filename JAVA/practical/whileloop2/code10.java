class While3 {
	public static void main(String[] args) {
		long sum = 0L;
		long rem = 0L;
		long num = 9307922405L;
		while(num > 0) {
			rem = num % 10;
			num = num / 10;
			sum = sum + rem;
		}
		System.out.println("Sum of digits in 9307922405 is "+sum);
	}
}

