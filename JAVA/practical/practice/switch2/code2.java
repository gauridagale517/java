class SwitchDemo2 {
	public static void main(String[] args) {
		char grade = 'O';
		switch(grade) {
			case 'O' :
				System.out.println("Outstanding..!!");
				break;
			case 'A' :
				System.out.println("Excellent..!!");
				break;
			case 'B' :
				System.out.println("Best..!!");
				break;
			case 'C' :
				System.out.println("Good..!!");
				break;
			case 'P' :
				System.out.println("Pass..!!");
				break;
			default :
				System.out.println("Enter correct grade :(");
		}
	}
}

