class SwitchDemo7 {
	public static void main(String[] args) {
		int budget = 10000;
		switch(budget) {
			case 15000 :
				System.out.println("For budget "+budget+" destination is Jammu and Kashmir");
				break;
			case 10000 :
                                System.out.println("For budget "+budget+" destination is Manali");
                                break;
			case 6000 :
                                System.out.println("For budget "+budget+" destination is Amritsar");
                                break;
			case 2000 :
                                System.out.println("For budget "+budget+" destination is Mahabaleshwar");
                                break;
			default :
                                System.out.println("For Other budget try next time :)");
                                break;
		}
	}
}
