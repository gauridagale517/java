class SwitchDemo6 {
        public static void main(String[] args) {
                int num = 5;
		if(num <= 5) {
			switch(num) {
				case 1 :
					System.out.println("One");
					break;
				case 2 :
                                        System.out.println("Two");
                                        break;
				case 3 :
                                        System.out.println("Three");
                                        break;
				case 4 :
                                        System.out.println("Four");
                                        break;
				case 5 :
                                        System.out.println("Five");
                                        break;
			}
		}
		else {
			switch(num) {
				case 6 :
                                        System.out.println(num+" is greter than 5");
                                        break;
				case 7 :
                                        System.out.println(num+" is greter than 5");
                                        break;
				case 8 :
                                        System.out.println(num+" is greter than 5");
                                        break;
				case 9 :
                                        System.out.println(num+" is greter than 5");
                                        break;
			}
		}
	}
}

