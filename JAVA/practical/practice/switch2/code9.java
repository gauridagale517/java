class SwitchDemo9 {
	public static void main(String[] args) {
		int phy = 80;
		int chem = 85;
		int oop = 75;
		int maths = 78;
		int cg = 79;
		int sum = 0;
		if(phy >= 40 && chem >= 40 && oop >= 40 && maths >= 40 && cg >= 40) {
			sum = phy + chem + oop + maths + cg;
			System.out.println("Total marks :"+sum);
		}
		else {
			System.out.println("You failed the exam :(");
		}
		char grade = 'F';
		if(sum>=450)
			grade = 'O';
	        if(sum>=370 && sum <450)
			grade = 'A';
		if(sum>=270 && sum < 370) 
			grade = 'B';
		if(sum>=200 && sum < 270)
			grade = 'P';
		switch(grade) {
			case 'O' :
				System.out.println("First class with distinction..!!");
				break;
			case 'A' :
                                System.out.println("First class..!!");
                                break;
			case 'B' :
                                System.out.println("Second class..!!");
                                break;
			case 'P' :
                                System.out.println("Pass..!!");
                                break;
			case 'F' :
				break;
		}
	}
}



