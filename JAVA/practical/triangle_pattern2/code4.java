import java.io.*;
class pattern4 {
        public static void main(String[] args) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no.of rows:");
                int rows = Integer.parseInt(br.readLine());
                for(int i=1; i<=rows; i++) {
                        int ch1 = 96+rows;
			int ch2 = 64+rows;
                        for(int j=1; j<=i; j++) {
                                if(i%2==0)
                                        System.out.print((char)ch2-- + " ");
                                else
                                        System.out.print((char)ch1-- +" ");
                        }
                        System.out.println();
                }
        }
}
