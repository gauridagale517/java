import java.io.*;
class pattern6 {
        public static void main(String[] args) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no.of rows:");
                int rows = Integer.parseInt(br.readLine());
                char ch = 65;
		for(int i=1; i<=rows; i++) {
			int num = 1;
                        for(int j=1; j<=i; j++) {
				if(i%2!=0) {
                                	System.out.print(num+" ");
                                	num++;
				}
				else 
					System.out.print(ch +" ");
				ch++;
                        }
			//ch++;
                        System.out.println();
                }
        }
}
