import java.io.*;
class pattern8 {
        public static void main(String[] args) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.print("Enter no.of rows:");
                int rows = Integer.parseInt(br.readLine());
                char ch = 'a';
                int num = 1;
                for(int i=1; i<=rows; i++) {
                        for(int j=1; j<=i; j++) {
                                if(j%2!=0) {
                                        System.out.print(j+" ");
                                }
                                else
                                        System.out.print(ch +" ");
                                ch++;
                        }
		//	ch--;
                        System.out.println();
                }
        }
}
