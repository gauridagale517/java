class Bitwise {
	public static void main(String[] args) {
		int a = 5;
		int b = 3;
		System.out.println(a&b);	// 0001
		System.out.println(a|b);	// 0111
		System.out.println(a^b);	// 0110
		System.out.println(a<<1);	// 1010
		System.out.println(a>>1);	// 0010
	}
}

