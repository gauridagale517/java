import java.util.*;
class Array6 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size = sc.nextInt();
                char arr[] = new char[size];
                System.out.println("Enter elements of an array : ");
                for(int i=0; i<arr.length ;i++) {
                        arr[i] = (char)sc.nextLine();
                }
                System.out.println("Elements of an array are : ");
                for(int i=0; i<arr.length; i++) {
                        System.out.println(arr[i]+ " ");
                }
        }
}
