class Prog3 {
	public static void main(String[] args) {
		char ch = 'u';
		if(ch=='a' || ch=='e' || ch=='i' || ch=='o' || ch=='u') {
			System.out.println(ch+" is a vowel in lowercase");
		}
		else if(ch=='A' || ch=='E' || ch=='I' || ch=='O' || ch=='U') {
			System.out.println(ch+" is a vowel in uppercase");
		}
		else {
			System.out.println(ch+" is a consonant");
		}
	}
}

