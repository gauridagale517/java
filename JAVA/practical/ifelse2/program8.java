class Prog8 {
	public static void main(String[] args) {
		float percentage = 85.50f;
		if(percentage>85.00 && percentage>=75.00 ) {
			 System.out.println("Passed:first class with distinction");
		}
		else if(percentage<=75.00 && percentage>=60.00) {
			 System.out.println("Passed:first class");
		}
		else if(percentage<=60.00 && percentage>=50.00) {
			 System.out.println("Passed:second class");
		}
		else {
			 System.out.println("Passed");
		}
	}
}

