class Pattern10 {
	public static void main(String[] args) {
		int num = 1;
		int row = 4;
		for(int i = 1; i<=row; i++) {
			for(int j=1; j<=row; j++) {
				System.out.print(num+" ");
				num++;
			}
			num-=3;
			System.out.println();
		}
	}
}

