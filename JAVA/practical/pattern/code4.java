class Pattern4 {
        public static void main(String[] args) {
                int ch = 65;
                int row = 3;
                for(int i = 1; i<=row; i++) {
                        for(int j=1; j<=row; j++) {
                                System.out.print((char)ch+" ");
                                ch+=2;
                        }
                        System.out.println();
                }
        }
}
