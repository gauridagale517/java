import java.util.*;
class Array4 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter elements of an aray : ");
                for(int i=0; i<arr.length; i++)
                        arr[i] = sc.nextInt();
		System.out.print("Enter element to check : ");
		int num = sc.nextInt();
		int count = 0;
		for(int i=0; i<arr.length; i++) {
			if(arr[i]==num) 
				count++;
		}
		if(count>2) 
			System.out.println(num+" occurred greater than two times in an array..");
		else if(count < 2) 
			System.out.println(num+" occurred less than two times in an array..");
		else 
			System.out.println(num+" occurred two times in an array..");
	}
}

