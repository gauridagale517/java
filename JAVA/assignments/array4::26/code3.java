import java.util.*;
class Array3 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter elements of an aray : ");
                for(int i=0; i<arr.length; i++)
                        arr[i] = sc.nextInt();
                int max = arr[0];
                for(int i=0; i<arr.length; i++) {
                        if(arr[i] > max)
                                max = arr[i];
                }
                int max2 = arr[0];
                for(int i=0; i<arr.length; i++) {
                        if(arr[i] >= max2 && arr[i] < max) {
                                max2 = arr[i];
                        }
                }
                System.out.println("Second largest element in an array is : "+max2);
        }
}
