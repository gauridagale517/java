import java.util.*;
class Array2 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter elements of an aray : ");
                for(int i=0; i<arr.length; i++)
                        arr[i] = sc.nextInt();
		int min = arr[0];
		for(int i=0; i<arr.length; i++) {
			if(arr[i] < min) 
				min = arr[i];
		}
		int max = arr[0];
		for(int i=0; i<arr.length; i++) {
			if(arr[i] > max) {
				max = arr[i];
			}
		}
		int diff;
		diff = max - min;
		System.out.println("Difference between min and max is : "+diff);
	}
}

