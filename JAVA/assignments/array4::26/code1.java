import java.util.*;
class Array1 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of an array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter elements of an aray : ");
		for(int i=0; i<arr.length; i++) 
			arr[i] = sc.nextInt();
		int avg = 0;
		int sum = 1;
		for(int i=0; i<arr.length; i++) {
			sum = sum + arr[i];
		}
		avg = sum / size;
		System.out.println("Average is : "+avg);
	}
}	
