import java.util.*;
class Arr5 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter elements of an array : ");
                for(int i=0; i<arr.length; i++)
                        arr[i] = sc.nextInt();
		int rem = 0;
		for(int i=0; i<arr.length; i++) {
			int count = 0;
			int num = arr[i];
			while(arr[i] > 0) {
				rem = arr[i]%10;
				arr[i] = arr[i]/10;
				if(rem > 0) 
					count++;
			}
			System.out.println("count of digits in " +num+" is " +count);
		}
	}
}


