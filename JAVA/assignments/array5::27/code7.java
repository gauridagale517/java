import java.util.*;
class Arr7 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter elements of an array : ");
                for(int i=0; i<arr.length; i++)
                        arr[i] = sc.nextInt();
		System.out.print("Composite numbers are : ");
                for(int i=0; i<arr.length; i++) {
                        int num = 1;
                        int count = 0;
                        while(num <= arr[i]) {
                                if(arr[i]%num == 0)
                                        count++;
                                num++;
                        }
                        if(count > 2) {
                                System.out.print(arr[i] + " ");
                        }
                }
        }
}
