import java.util.*;
class Pattern10 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter number : ");
		long num = sc.nextLong();
		long number = num;
		long rem = 0L;
		long rev = 0L;
		while(num>0) {
			rem = num%10;
			num = num/10;
			rev = rev*10+rem;
		}
		while(rev>0) {
			long square = 1L;
			rem = rev%10;
                        rev = rev/10;
			if(rem%2==1) { 
				square = rem*rem;
				System.out.print(square+",");
			}
		}
		System.out.println();
	}
}

