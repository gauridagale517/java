import java.util.*;
class Pattern4 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		for(int i=1; i<=rows ; i++) {
			int num = rows-i+1;
			for(int j=1; j<=i; j++) {
				System.out.print(j*num +" ");
			}
			System.out.println();
		}
	}
}


