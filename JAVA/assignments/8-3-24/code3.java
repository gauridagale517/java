import java.util.*;
class Pattern3 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows : ");
		int rows = sc.nextInt();
		for(int i=1; i<=rows; i++) {
			int ch = 64+rows;
			for(int j=1; j<=rows; j++) {
				if(i%2==0) 
					System.out.print(j+" ");
				else 
					System.out.print((char)ch-- +" ");
			}
			System.out.println();
		}
	}
}



