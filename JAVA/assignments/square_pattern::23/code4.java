import java.util.*;
class Pattern4 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter no.of row : ");
                int rows = sc.nextInt();
                for(int i=1; i<=rows; i++) {
                        for(int j=1; j<=rows; j++) {
                                if(i%2==0 && j%2==0) {
                                        System.out.print("$"+" ");
                                }
                                else {
                                        System.out.print("&"+" ");
                                }
                        }
                        System.out.println();
                }
        }
}
