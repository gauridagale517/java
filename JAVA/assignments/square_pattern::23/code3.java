import java.util.*;
class Pattern3 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter no.of row : ");
                int rows = sc.nextInt();
                int num = rows;
                int ch = 96+rows;
                for(int i=1; i<=rows; i++) {
                        for(int j=1; j<=rows; j++) {
                                if(num%2==1) {
                                        System.out.print((char)ch+" ");
                                        //ch1++;
                                }
                                else {
                                        System.out.print(num+" ");
                                        //ch2++;
                                }
                                num++;
                                ch++;
                        }
                        System.out.println();
                }
        }
}
