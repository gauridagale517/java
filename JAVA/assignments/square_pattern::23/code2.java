import java.util.*;
class Pattern1 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter no.of row : ");
                int rows = sc.nextInt();
                int ch1 = 64+rows;
                int ch2 = 96+rows;
                for(int i=1; i<=rows; i++) {
                        for(int j=1; j<=rows; j++) {
                                if(j==1)
                                        System.out.print((char)ch1+" ");
                                else
                                        System.out.print((char)ch2+" ");
                                ch1++;
                                ch2++;
                        }
                        System.out.println();
                }
        }
}
