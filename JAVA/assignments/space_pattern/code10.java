import java.util.*;
class Space10 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter no. of rows : ");
                int rows = sc.nextInt();
                int num = 65;
                for(int i = 1; i<=rows; i++) {
                        for(int sp = 1; sp<i; sp++) {
                                System.out.print("\t");
                        }
                        for(int j=i; j<=rows; j++) {
                                if(j%2==1)
					System.out.print(num++ + "\t" );
				else
					System.out.print((char)num++ +"\t");
                        }
                        num = num-rows+i;
                        System.out.println();
                }
        }
}
