import java.util.*;
class Space7 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter no.of rows : ");
		int rows = sc.nextInt();
                for(int i = 1; i<=rows; i++) {
                        for(int sp = 1; sp<i; sp++) {
                                System.out.print(" "+" ");
                        }
                	int num = 1;
                        for(int j=rows; j>=i; j--) {
                                System.out.print(num+ " " );
				num++;
                        }
                        System.out.println();
                }
        }
}
