import java.util.*;
class invert9 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter no.of rows : ");
                int rows = sc.nextInt();
                for(int i=1; i<=rows; i++) {
                        for(int sp=1; sp<i; sp++)
                                System.out.print("\t");
                        for(int j=1; j<=(rows-i)*2+1 ;j++) {
				if(j%2==1) 
					System.out.print("1"+"\t");
				else 
					System.out.print("0"+"\t");
			}
                        System.out.println();
                }
        }
}
