import java.util.*;
class MixedPattern5 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                int rows = sc.nextInt();
                int num = rows;
                for(int i=1; i<=rows; i++) {
                        for(int j=1; j<=rows; j++) {
				if(i%2==1) 
					System.out.print(num*num +"\t");
				else 
					System.out.print(num +"\t");
                                num++;
                        }
                        System.out.println();
                }
        }
}
