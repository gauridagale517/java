import java.util.*;
class MixedPattern4 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter no.of rows : ");
		int rows = sc.nextInt();
                int ch = 'A';
		int ch1 = 'a';
                for(int i=1; i<=rows; i++) {
                        for(int space=1; space<=i; space++) {
                                System.out.print("\t");
                        }
                        for(int j=rows; j>=i; j--) {
				if(rows%2==1) {
                                	System.out.print((char)ch + "\t");
                                	ch++;
				}
				else {
					System.out.print((char)ch1 + "\t");
					ch1++;
				}
                        }
			ch = ch-rows+i;
			ch1 = ch1-rows+i;
                        System.out.println();
                }
        }
}
