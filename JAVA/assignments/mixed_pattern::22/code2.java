import java.util.*;
class MixedPattern2 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter no.of rows : ");
		int rows = sc.nextInt();
		int num = 1;
                for(int i=1; i<=rows; i++) {
                        for(int space=1; space<=i; space++) {
                                System.out.print("\t");
                        }
                        for(int j=rows; j>=i; j--) {
                                System.out.print(num + "\t");
                                num++;
                        }
			num--;
                        System.out.println();
                }
        }
}
