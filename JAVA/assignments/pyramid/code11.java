import java.util.*;
class Pyramid11 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter no. of rows : ");
                int rows = sc.nextInt();
                for(int i = 1; i<=rows; i++) {
                        char ch1 = 'A';
			char ch2 = 'a';
                        for(int sp = rows; sp>i; sp--) {
                                System.out.print("\t");
                        }
                        for(int j=1; j<=i*2-1; j++) {
				if(i%2==1) {
                                	if(j<i)
                                        	System.out.print(ch1++ + "\t" );
                                	else
                                        	System.out.print(ch1-- + "\t" );
				}
				else {
					if(j<i)
                                                System.out.print(ch2++ + "\t" );
                                        else
                                                System.out.print(ch2-- + "\t" );
				}
                        }
                        System.out.println();
                }
        }
}
