class While8 {
        public static void main(String[] args) {
                int rem = 0;
                int num = 256985;
		int product = 1;
                while(num > 0) {
                        rem = num % 10;
                        num = num / 10;
                        if(rem%2!=0)
				product = product * rem;
		}
                System.out.print("Product of odd digits:"+product);
                }
        }

