class While5 {
        public static void main(String[] args) {
                int rem = 0;
                int num = 256985;
                while(num > 0) {
                        rem = num % 10;
                        num = num / 10;
                        if(rem%2==0)
                                System.out.print(rem * rem * rem+" ");
                }
        }
}
