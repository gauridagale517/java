class While6 {
        public static void main(String[] args) {
                int rem = 0;
                int num = 234;
		int product = 1;
                while(num > 0) {
                        rem = num % 10;
                        num = num / 10;
			product = product * rem;
		}
                System.out.print(product+" ");
	}
}

