class While10 {
        public static void main(String[] args) {
                int rem = 0;
                int num = 9367924;
		int sum = 0;
		int product = 1;
                while(num > 0) {
                        rem = num % 10;
                        num = num / 10;
                        if(rem%2!=0)
				sum = sum + rem;
			else
				product = product * rem;
		}
		System.out.println("Sum of odd digits:"+sum);
		System.out.println("Product of even digits:"+product);
	}
}

