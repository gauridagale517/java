




import java.util.*;
class P2{
	public static void main(String[] t){
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number :");
		int num = sc.nextInt();
		int onum = num;
		int sum = 0;
		while(num !=0){
			int digit = num%10;
			sum += fact1(digit);
			num /=10;
		}
		if(sum == onum){
			System.out.println(onum+" is a strong number");
		}else{
			System.out.println(onum+" is not strong number");
		}
	}
	public static int fact1(int num){
		int fact = 1;
		while(num != 0){
			fact *= num;
			num--;
		}
		return fact;
	}
}
