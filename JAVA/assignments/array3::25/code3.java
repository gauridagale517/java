import java.util.*;
class Array3 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter elements of an array : ");
                for(int i=0; i<arr.length; i++) {
                        arr[i] = sc.nextInt();
                }
                System.out.print("Enter a number : ");
                int num = sc.nextInt();
		int count = 0;
                for(int i=0; i<arr.length; i++) {
                        if(arr[i]==num) {
				count++;
                        }		
		}
		if(count==0) 
			System.out.println("num "+num+" is not found..:(");
		else 
			System.out.println("num "+num+" occured "+count+" times in an array");
	}
}

