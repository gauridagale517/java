import java.util.*;
class Array2 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter elements of an array : ");
                for(int i=0; i<arr.length; i++) {
                        arr[i] = sc.nextInt();
                }
		System.out.print("Enter a number : ");
		int num = sc.nextInt();
		int count = 0;
		for(int i=0; i<arr.length; i++) {
			if(arr[i]==num) {
				count++;
				System.out.println("num "+num+" first occured at index : "+i);
				break;
			}
		}
		if(count==0) 
			System.out.println("num "+num+" is not found :(");
	}
}

