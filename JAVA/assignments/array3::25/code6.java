import java.util.*;
class Array6 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter elements of an array : ");
                for(int i=0; i<arr.length; i++) {
                        arr[i] = sc.nextInt();
                }
		for(int i=0; i<arr.length; i++) {
			if(arr[i]%2==0) {
				arr[i] = 0;
				System.out.print(arr[i] + " ");
			}
			else {
				arr[i] = 1;
				System.out.print(arr[i] + " ");
			}
		}
		System.out.println();
	}
}

