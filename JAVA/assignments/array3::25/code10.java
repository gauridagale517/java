import java.util.*;
class Array10 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                int product = 1;
		System.out.print("Enter elements of an array : ");
                for(int i=0; i<arr.length; i++) {
                        arr[i] = sc.nextInt();
                }
                
                for(int i=0; i<arr.length; i++) {
                        int num = 1;
                        int count = 0;
		
                        while(num<=arr[i]) {
                                if(arr[i]%num==0)
                                        count++;
                                num++;
                        }
                        if(count<=2)
                                product = product*arr[i];
                }
                System.out.println(product);
        }
}
