import java.util.*;
class Pattern7 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter rows:");
                int rows = sc.nextInt();
                for(int i = 1; i<=rows; i++) {
                        int num = rows-i+1;
                        int ch = 96+rows-i+1;
                        for(int j = 1; j<=rows-i+1; j++) {
                                if(j%2==1)
                                        System.out.print(num +" ");
                                else
                                        System.out.print((char)ch +" ");
				ch--;
				num--;
                        }
                        System.out.println();
                }
        }
}
