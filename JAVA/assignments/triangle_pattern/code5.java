import java.util.*;
class Pattern5 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter rows:");
                int rows = sc.nextInt();
                int num = 1;
                for(int i = 1; i<=rows; i++) {
			char ch = 'A';
			char ch2 = 'a';
                        for(int j = i; j<=rows; j++) {
				if(i%2==0)
					System.out.print(ch2++ +" ");
				else 
					System.out.print(ch++ +" ");

                        }
                        System.out.println();
                }
        }
}
