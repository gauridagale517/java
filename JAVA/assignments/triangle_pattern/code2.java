import java.util.*;
class Pattern2 {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter rows:");
		int rows = sc.nextInt();
		int num = 1;
		for(int i = 1; i<=rows; i++) {
			for(int j = i; j<=rows; j++) {
				System.out.print(num*2+" ");
				num++;
			}
			System.out.println();
		}	
	}
}
