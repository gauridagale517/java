import java.util.*;
class Pattern3 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter rows:");
                int rows = sc.nextInt();
                int num = rows+1;
		int product = num*rows;
                for(int i = 1; i<=rows; i++) {
                        for(int j = i; j<=rows; j++) {
                                System.out.print(product+" ");
                                product-=2;
                        }
                        System.out.println();
                }
        }
}
