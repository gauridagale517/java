import java.util.*;
class array4 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter elements of an array : ");
                for(int i=0; i<arr.length; i++)
                        arr[i] = sc.nextInt();
                System.out.print("Enter element to be searched : ");
		int key = sc.nextInt();
                for(int i=0; i<arr.length; i++) {
                        if(arr[i]==key) {
                                System.out.println(arr[i]+" found at index "+i);
                        }
                }
        }
}
