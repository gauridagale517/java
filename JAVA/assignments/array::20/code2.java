import java.util.*;
class array2 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter elements of an array : ");
                for(int i=0; i<arr.length; i++)
                        arr[i] = sc.nextInt();
                int sum = 0;
                System.out.print("Elements divisible by 3 are : ");
                for(int i=0; i<arr.length; i++) {
                        if(arr[i]%3==0) {
                                System.out.print(arr[i] + " ");
                                sum = sum+arr[i];
                        }
                }
		System.out.println();
                System.out.println("Sum of elements divisible by 3 is : "+sum);
        }
}
