import java.util.*;
class array5 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter elements of an array : ");
                for(int i=0; i<arr.length; i++)
                        arr[i] = sc.nextInt();
                int sum = 0;
                System.out.print("Odd indexed elements are : ");
                for(int i=0; i<arr.length; i++) {
                        if(i%2==1) {
                                System.out.print(arr[i] + " ");
                                sum = sum+arr[i];
                        }
                }
		System.out.println();
                System.out.println("Sum of odd indexed elements is : "+sum);
        }
}
