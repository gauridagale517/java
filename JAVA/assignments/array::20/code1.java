import java.util.*;
class array {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter size of an array : ");
		int size = sc.nextInt();
		int arr[] = new int[size];
		System.out.println("Enter elements of an array : ");
		for(int i=0; i<arr.length; i++) 
			arr[i] = sc.nextInt();
		int count = 0;
		System.out.print("Even elements : ");
		for(int i=0; i<arr.length; i++) {
			if(arr[i]%2==0) {
				System.out.print(arr[i] + " ");
				count++;
			}
		}
		System.out.println("Count of even elements is : "+count);
	}
}


