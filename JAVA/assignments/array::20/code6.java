import java.util.*;
class array6 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size = sc.nextInt();
                int arr[] = new int[size];
                System.out.println("Enter elements of an array : ");
                for(int i=0; i<arr.length; i++)
                        arr[i] = sc.nextInt();
                int product = 1;
                System.out.print("Odd indexed elements are : ");
                for(int i=0; i<arr.length; i++) {
                        if(i%2==1) {
                                System.out.print(arr[i] + " ");
                                product = product*arr[i];
                        }
                }
                System.out.println();
                System.out.println("Product of odd indexed elements is : "+product);
        }
}
