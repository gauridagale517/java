import java.util.*;
class Arr6 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size = sc.nextInt();
                int count = 0;
                int arr[] = new int[size];
                System.out.println("Enter elements of an array : ");
                for(int i=0; i<arr.length; i++)
                        arr[i] = sc.nextInt();
		System.out.print("Enter key element : ");
		int key = sc.nextInt();
		for(int i=0; i<arr.length; i++) {
			if(arr[i]%key==0) 
				System.out.println("An element multiple of "+key+" found at index "+i);
		}
	}
}
