import java.util.*;
class Arr10 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size = sc.nextInt();
                int count = 0;
                int arr[] = new int[size];
                System.out.println("Enter elements of an array : ");
                for(int i=0; i<arr.length; i++)
                        arr[i] = sc.nextInt();
		int max1 = arr[0];
		for(int i=0; i<arr.length; i++) {
			if(arr[i] > max1)
				max1 = arr[i];
		}
		int max2 = arr[0];
		for(int i=0; i<arr.length; i++) {
			if(arr[i]>=max2 && arr[i] < max1) 
				max2 = arr[i];
		}
		int max3 = arr[0];
		for(int i=0; i<arr.length; i++) {
			if(arr[i]>=max3 && arr[i] < max2) 
				max3 = arr[i];
		}
		System.out.println("third largest element is " + max3);
	}
}
