import java.util.*;
class Arr3 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter size of an array : ");
                int size = sc.nextInt();
                int count = 0;
		int arr[] = new int[size];
                System.out.println("Enter elements of an array : ");
                for(int i=0; i<arr.length; i++)
                        arr[i] = sc.nextInt();
		System.out.print("Enter key element : ");
		int key = sc.nextInt();
		for(int i=0; i<arr.length; i++) {
			if(arr[i]==key) 
				count++;
			if(count > 2) 
				System.out.print(arr[i]*arr[i]*arr[i]);
			else
				System.out.print(arr[i]+" ");
		}
		System.out.println();
	}
}

