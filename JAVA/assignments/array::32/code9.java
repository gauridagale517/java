import java.util.*;
class twoDArray9 {
       public static void main(String[] args) {
               Scanner sc = new Scanner(System.in);
               System.out.print("Enter rows and columns : ");
               int size = sc.nextInt();
               int arr[][] = new int[size][size];
               int sum1 = 0;
	       int sum2 = 0;
	       int product = 1;
               System.out.println("Enter array elements : ");
               for(int i=0; i<size; i++) {
                       for(int j=0; j<size; j++) {
                               arr[i][j] = sc.nextInt();
                       }
               }
               System.out.println("Array elements are : ");
               for(int i=0; i<size; i++) {
                       for(int j=0; j<size; j++) {
			       if(i==j)
			           sum1 = sum1 + arr[i][j];
                               if((i+j)==(size-1))
                                   sum2 = sum2 + arr[i][j];
                       }
		       product = sum1 * sum2;
               }
               System.out.println("Sum of peimary diagonal elements : " +sum1);
	       System.out.println("Sun of secondary diagonal elements : "+sum2);
	       System.out.println("Product is :"+product);
       }
}
