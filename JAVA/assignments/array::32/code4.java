import java.util.*;
class twoDArray4 {
       public static void main(String[] args) {
               Scanner sc = new Scanner(System.in);
               System.out.print("Enter rows : ");
               int rows = sc.nextInt();
               System.out.print("Enter columns : ");
               int columns= sc.nextInt();
               int arr[][] = new int[rows][columns];
               //int sum1 = 0;
               System.out.println("Enter array elements : ");
               for(int i=0; i<rows; i++) {
                       for(int j=0; j<columns; j++) {
                               arr[i][j] = sc.nextInt();
                       }
               }
               System.out.println("Array elements are : ");
               for(int i=0; i<rows; i++) {
                       int sum1 = 0;
		       for(int j=0; j<columns; j++) {
			       if(i%2==0) 
                                   sum1 = sum1 + arr[i][j];
                       }
		       if(i%2==0) 
                           System.out.println("sum of row "+(i+1)+" is : "+sum1);
               }
       }
}
