import java.util.*;
class twoDArray8 {
       public static void main(String[] args) {
               Scanner sc = new Scanner(System.in);
               System.out.print("Enter rows and columns : ");
               int size = sc.nextInt();
               int arr[][] = new int[size][size];
               int sum = 0;
               System.out.println("Enter array elements : ");
               for(int i=0; i<size; i++) {
                       for(int j=0; j<size; j++) {
                               arr[i][j] = sc.nextInt();
                       }
               }
               System.out.println("Array elements are : ");
               for(int i=0; i<size; i++) {
                       for(int j=0; j<size; j++) {
                               if((i+j)==(size-1))
                                   sum = sum + arr[i][j];
                       }
               }
               System.out.println(sum);
       }
}
