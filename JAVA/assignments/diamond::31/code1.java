import java.util.*;
class Diamond {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int rows = sc.nextInt();
		int col = 0;
		int space = 0;
		for(int i=0; i<rows*2; i++) {
			if(i<=rows) {
				space = rows-i;
				col = i*2-1;
			}
			else {
				space = i-rows;
				col -= 2;
			}
			for(int sp = 1; sp <= space; sp++) 
				System.out.print("\t");
			for(int j = 1; j <= col; j++) 
				System.out.print("# \t");
			System.out.println();
		}
	}
}
