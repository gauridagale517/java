import java.util.*;
class Prime {
        public static void main(String[] args) {
               Scanner sc = new Scanner(System.in);
               System.out.print("Enter number : ");
               int num = sc.nextInt();
               int number = num;
               int i = 1;
               int count = 0;
               while(i<=num) {
                       if(num%i==0)
                               count++;
                       i++;
               }
               if(count>2)
                       System.out.println(number+" is not a prime number");
               else
                       System.out.println(number+" is a prime number");
        }
}
