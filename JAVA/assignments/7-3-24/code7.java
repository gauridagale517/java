import java.util.*;
class Reverse {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number:");
		int num = sc.nextInt();
		int number = num;
		int rem = 0;
		System.out.print("Reverse of "+number+" is:");
		while(num > 0) {
			rem = num%10;
			num = num/10;
			System.out.print(rem);
		}
		System.out.println();
	}
}


