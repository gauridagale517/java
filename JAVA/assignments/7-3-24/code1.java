import java.util.*;
class Factors {
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		System.out.print("Enter number : ");
		int num = sc.nextInt();
		System.out.print("Factors of "+num+" are : ");
		int i = 1;
		while(i<=num) {
			if(num%i==0) 
				System.out.print(i+",");
			i++;
		
		}
		System.out.println();
	}
}


