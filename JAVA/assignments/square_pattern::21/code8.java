import java.util.*;
class SquarePattern8 {
        public static void main(String[] args) {
               Scanner sc = new Scanner(System.in);
               System.out.print("Enter no. of rows : ");
               int rows = sc.nextInt();
               int num = rows;
               for(int i=1; i<=rows; i++) {
		       int ch = 64+rows;
                       for(int j=1; j<=rows; j++) {
                               if(num%2==1)
                                       System.out.print("#" +" ");
                               else {
                                       System.out.print((char)ch +" ");
				       ch--;
			       }
                               num++;
                       }
                      System.out.println();
               }
        }
}
