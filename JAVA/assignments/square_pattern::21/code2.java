import java.util.*;
class SquarePattern2 {
        public static void main(String[] args) {
               Scanner sc = new Scanner(System.in);
               System.out.print("Enter no. of rows : ");
               int rows = sc.nextInt();
	       int num = rows;
               for(int i=1; i<=rows; i++) {
                       for(int j=1; j<=rows; j++) {
                               if(j==1) {
                                       System.out.print(num*num +" ");
				       num++;
			       }
                               else {
                                       System.out.print(num +" ");
				       num++;
			       }
                       }
                      System.out.println();
               }
        }
}
