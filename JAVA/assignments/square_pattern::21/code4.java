import java.util.*;
class SquarePattern4 {
        public static void main(String[] args) {
               Scanner sc = new Scanner(System.in);
               System.out.print("Enter no. of rows : ");
               int rows = sc.nextInt();
	       int ch = 64+rows;
	       int num = rows;
               for(int i=1; i<=rows; i++) {
                       for(int j=1; j<=rows; j++) {
                               if(j==1)
                                       System.out.print((char)ch +" ");
                               else
                                       System.out.print(num +" ");
			       ch++;
			       num++;
                       }
                      System.out.println();
               }
        }
}
