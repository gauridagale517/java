import java.util.*;
class SquarePattern7 {
        public static void main(String[] args) {
               Scanner sc = new Scanner(System.in);
               System.out.print("Enter no. of rows : ");
               int rows = sc.nextInt();
               int num = rows;
	       char ch = 'A';
               for(int i=1; i<=rows; i++) {
                       for(int j=1; j<=rows; j++) {
                               if(num%2==0)
                                       System.out.print(num +" ");
                               else
                                       System.out.print(ch +" ");
                               num++;
                       }
		      ch++;
                      System.out.println();
               }
        }
}
