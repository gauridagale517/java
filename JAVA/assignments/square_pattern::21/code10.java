import java.util.*;
class SquarePattern10 {
        public static void main(String[] args) {
               Scanner sc = new Scanner(System.in);
               System.out.print("Enter no. of rows : ");
               int rows = sc.nextInt();
               for(int i=1; i<=rows; i++) {
                       int num = rows;
		       int ch = 64+rows;
		       for(int j=1; j<=rows; j++) {
                               if(i%2==1) {
                                       if(num%2==1)
                                                System.out.print(num +" ");
                                       else
                                               System.out.print((char)ch +" ");
                               }
                               else {
                              		System.out.print((char)ch + " ");
			       }
			       num--;
			       ch--;
                       }
                      System.out.println();
               }
        }
}
