import java.util.*;
class SideTriangle9 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter no.of rows : ");
                int rows = sc.nextInt();
                int col=0;
		int num = 0;
                for(int i=1; i<rows*2; i++) {
                        if(i<=rows)
                                col = rows-i;
                        else
                                col = i-rows;

                        for(int sp=1; sp<=col; sp++)
                                System.out.print("\t");

                        if(i<=rows)
                                col = i;
                        else
                                col = rows*2-i;

                        if(i<=rows) 
				num = rows-i;
			else 
				num --;
			for(int j=1; j<=col; j++) {
                                System.out.print(num++ +"\t");
			}
			//num--;
			System.out.println();
                }
        }
}
