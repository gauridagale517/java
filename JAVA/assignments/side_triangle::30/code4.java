import java.util.*;
class SideTriangle4 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter no.of rows : ");
                int rows = sc.nextInt();
                int col=0;
                //int num = 0;
                for(int i=1; i<rows*2; i++) {
                        if(i<=rows)
                                col = i;
                        else
                                col = rows*2-i;
                        int num = rows;
			for(int j=1; j<=col; j++) {
				if(i<=rows) 
					num = rows-i+1;
				else 
					num = 1+i-rows;
				System.out.print(num+"\t");	
                        }
                        System.out.println();
                }
        }
}
