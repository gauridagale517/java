import java.util.*;
class SideTriangle6 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.print("Enter no.of rows : ");
                int rows = sc.nextInt();
                int col=0;
                for(int i=1; i<rows*2; i++) {
                        if(i<=rows)
                                col = rows-i;
                        else
                                col = i-rows;
			
			for(int sp=1; sp<=col; sp++)  
				System.out.print("\t");
		        
			if(i<=rows) 
				col = i;
			else 
				col = rows*2-i;
                	
			for(int j=1; j<=col; j++) 
                        	System.out.print(j+"\t");
                	System.out.println();
                }
        }
}
