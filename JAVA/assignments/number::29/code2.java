import java.util.*;
class Number2 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                int num = sc.nextInt();
                int fact = 1;
		int rem = 0;
		int sum = 0;
                while(num>0) {
			rem = num%10;
			while(rem>0) {
				fact = fact*rem;
				rem--;
			}
			sum = sum + fact;
			num = num/10;
                }
                if(sum==num) {
                        System.out.println(num+" is strong number..");
                }
                else
                        System.out.println(num+" is not strong number..");
        }
}
