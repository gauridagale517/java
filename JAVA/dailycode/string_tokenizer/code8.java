import java.io.*;
class InputDemo8 {
        public static void main(String[] args) throws IOException {
                BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
                System.out.println("Enter your name:");
                String name = br.readLine();
                System.out.println("Enter society name:");
                String scName = br.readLine();
                System.out.println("Enter wing:");
                char wing = (char)br.read();
                System.out.println("Enter flatNo:");
                int flatNo = Integer.parseInt(br.readLine());
                System.out.println("Name:" +name);
                System.out.println("SocName:" +scName);
                System.out.println("Wing:" +wing);
                System.out.println("flatNo:" +flatNo);
        }
}
