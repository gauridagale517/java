class Parent {
	void fun() {
		System.out.println("In fun - Parent");
	}
}
class Child extends Parent {
	void fun() {
		System.out.println("In fun-child");
	}
	void run() {
		System.out.println("In run - child");
	}
	public static void main(String[] args) {
		Child obj = new Parent();
	}
}


