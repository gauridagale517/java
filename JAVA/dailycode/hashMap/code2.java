import java.util.*;

class HashMapMethods2 {
	public static void main(String[] args) {
		HashMap hm = new HashMap();
		hm.put(7,"MSDhoni");
		hm.put(18,"ViratKohli");
		hm.put(45,"RohitSharma");
		hm.put(1,"KLRahul");

		System.out.println(hm);

		hm.putIfAbsent(77,"ShubhmanGill");
		System.out.println(hm);

		hm.getOrDeFault(7,"MSD");
		
		//System.out.println(hm.capacity());

		System.out.println(hm.entrySet());

		System.out.println(hm.keySet());
	}
}

