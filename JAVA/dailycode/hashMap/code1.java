import java.util.*;
class HashMapMethods {
	public static void main(String[] args) {
		Map m = new HashMap();
		m.put("D","Amazon");
		m.put("A","Microsoft");
		m.put("C","Apple");
		m.put("B","Google");
		m.put("E","Google");

		System.out.println(m);

		System.out.println(m.remove("C"));

		System.out.println(m.get("B"));
		
		System.out.println(m.containsKey("C"));

		System.out.println(m.containsValue("Microsoft"));

		System.out.println(m.equals("Microsoft"));

		m.putIfAbsent("C","Infosys");
		System.out.println(m);

		m.replace("F","Infosys");
		System.out.println(m);
		
		System.out.println(m.isEmpty());

	}
}

