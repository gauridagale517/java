class StringDemo{
    public static void main(String[] args){

        String str1 = "Rohit";
        String str2 = str1;

        String str3 = new String("Virat");
        String str4 = str3;

        String str5 = new String(str1);
    }
}
