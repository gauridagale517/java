class Demo {
	Demo() {
		System.out.println("welcome Demo");
	}
}
class Demo2 extends Demo {
}
class Demo3 extends Demo2 {
	Demo3() {
		super();
		System.out.println("Demo e3");
	}
}
class Client {
	public static void main(String[] args) {
		Demo obj = new Demo3();
	}
}
