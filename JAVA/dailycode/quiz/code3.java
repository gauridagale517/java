class c2w {
	c2w(c2w obj) {
		this(10);
		System.out.println(obj);
		System.out.println("IN C2w");
	}
	c2w(int x) {
		System.out.println("In c2w " + x);
	}
	public String toString() {
		return "This is a C2w obj";
	}
	public static void main(String[] args) {
		c2w obj = new c2w(1);
		c2w obj1 = new c2w(obj);
		System.out.println(obj);
		System.out.println(obj1);
	}
}
