class Switch2 {
        public static void main(String[] args) {
                int num = 5;
                System.out.println("Before switch..");
                switch(num) {
                        case 1:
                                System.out.println("ONE");
                        case 2:
                                System.out.println("TWO");
                        case 3:
                                System.out.println("THREE");
                }
                System.out.println("After switch");
        }
}
