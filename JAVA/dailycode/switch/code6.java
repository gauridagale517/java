class Switch5 {
        public static void main(String[] args) {
                int num = 7;
                System.out.println("Before switch..");
                switch(num) {
                        case 1:
                                System.out.println("ONE");
				break;
                        case 2:
                                System.out.println("TWO");
				break;
                        case 3:
                                System.out.println("THREE");
				break;
                        default :
                                System.out.println("In default state..");
                }
                System.out.println("After switch");
        }
}
