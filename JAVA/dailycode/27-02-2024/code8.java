import java.util.*;
class Input8 {
        public static void main(String[] args) {
                Scanner sc = new Scanner(System.in);
                System.out.println("Enter first number:");
                int num1 = sc.nextInt();
                System.out.println("Enter second number:");
                int num2 = sc.nextInt();
		int sum = 0;
                for(int i=num1; i<=num2; i++)
                        sum = sum+i;
		System.out.println("Sum of numbers between "+num1+" &"+num2+" is = "+sum);
        }
}
